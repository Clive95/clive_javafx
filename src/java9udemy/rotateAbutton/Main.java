package java9udemy.rotateAbutton;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.FlowPane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

public class Main extends Application implements EventHandler {
    private Button rotateButton;
    private Button blurButton;
    private Button scaleButton;

    private Rotate rotate;
    private double angle;

    private BoxBlur blur;
    private double blurValue;

    private Scale scale;
    private double scaleFactor;

    private Reflection reflection;
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Rotate/Blur/Scale/Reflect");

        rotateButton = new Button("Rotate");
        blurButton = new Button("Blur");
        scaleButton = new Button("Scale");

        //Register our buttons. For handle().
        rotateButton.setOnAction(this);
        blurButton.setOnAction(this);
        scaleButton.setOnAction(this);

        // Rotate
        rotate = new Rotate();
        angle = 0.0;

        // Blur
        blur = new BoxBlur(1.0,1.0,1);
        blurValue = 1.0;

        // Scale
        scaleFactor = 0.4;
        scale = new Scale(scaleFactor, scaleFactor);

        // Reflection
        reflection = new Reflection();

        Label reflect = new Label("Reflection Adds Visual Sparkle");

        // Scene, Window
        FlowPane root = new FlowPane(15, 15);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(rotateButton, blurButton, scaleButton, reflect);

        Scene scene = new Scene(root, 300, 150);
        primaryStage.setScene(scene);
        primaryStage.show();

        //Setup transformations
        rotateButton.getTransforms().add(rotate);

        scaleButton.getTransforms().add(scale);

        reflection.setTopOpacity(0.7);
        reflection.setBottomOpacity(0.3);
        reflect.setEffect(reflection);
    }

    @Override
    public void handle(Event event) {
        if(event.getSource().equals(rotateButton)){
            angle += 15;
            rotate.setAngle(angle);
            rotate.setPivotX(rotateButton.getWidth() / 2);
            rotate.setPivotY(rotateButton.getHeight() / 2);
        }

        if(event.getSource().equals(blurButton)){
            if(blurValue == 10.0){
                blurValue = 1.0;
                blurButton.setEffect(null);
                blurButton.setText("Blur off");
            }else{
                blurValue++;
                blurButton.setEffect(blur);
                blurButton.setText("Blur on");
            }
            blur.setWidth(blurValue);
            blur.setHeight(blurValue);
        }

        if(event.getSource().equals(scaleButton)){
            scaleFactor += 0.1;
            if(scaleFactor > 2.0) scaleFactor = 0.4;
            scale.setX(scaleFactor);
            scale.setY(scaleFactor);
        }
    }
}
