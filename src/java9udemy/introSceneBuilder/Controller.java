package java9udemy.introSceneBuilder;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Button clickMeButton;
    @FXML
    private Label labelid;

    @FXML
    private JFXButton materialButton;


    @FXML
    private JFXTextField inputText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        materialButton.setOnAction(event -> {
            String textString = inputText.getText().trim();
            labelid.setText(textString);
        });
    }
}
