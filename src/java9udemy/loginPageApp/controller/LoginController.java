package java9udemy.loginPageApp.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {

    private static final String CORRECT_LOGIN = "CliveLewis";
    private static final String CORRECT_PASSWORD = "Persik502";
    @FXML
    private JFXTextField loginUsername;

    @FXML
    private JFXPasswordField loginPassword;

    @FXML
    private JFXButton loginButton;

    @FXML
    private Label loginWarning;

    @FXML
    void initialize(){

        loginUsername.setStyle("-fx-text-inner-color: #ffffff");
        loginPassword.setStyle("-fx-text-inner-color: #ffffff");
        loginButton.setOnAction(event -> {
            loginUser();
        });
    }

    private void loginUser(){

        if(!loginUsername.getText().trim().equals("")&&
        !loginPassword.getText().trim().equals("")){
            if(loginUsername.getText().equals(CORRECT_LOGIN)&&
            loginPassword.getText().equals(CORRECT_PASSWORD)){
                goToDetailsScreen();
            }
            else{
                loginWarning.setText("Incorrect username or password");
            }
        }
        else{
            loginWarning.setText("Please enter login and password");
        }
    }

    private void goToDetailsScreen(){
        try {
            loginButton.getScene().getWindow().hide();
            Stage detailsStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/java9udemy/loginPageApp/view/details.fxml"));
            loader.load();

            Parent root = loader.getRoot();
            Scene scene = new Scene(root);

            detailsStage.setScene(scene);
            detailsStage.show();
            detailsStage.setResizable(false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
