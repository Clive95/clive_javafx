package java9udemy.loginPageApp.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class DetailsController {

    @FXML
    private Label detailsWelcomeLabel;

    @FXML
    private Label detailsName;

    @FXML
    private Label detailsAge;

    @FXML
    private Label detailsProfession;

    @FXML
    private Button buttonFacebook;

    @FXML
    private Button buttonLinkedIn;

    @FXML
    private Button buttonYoutube;

    @FXML
    private Button buttonEmail;

    @FXML
    void initialize(){
        detailsName.setText("Name: Clive Lewis");
        detailsAge.setText("Age: 23");
        detailsProfession.setText("Profession: Programmer");
    }

    void setName(String name){
        detailsName.setText("Name: " + name);
    }
}
