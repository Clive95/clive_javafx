package thenewboston.alertbox;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage window;
    private Button button;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Alert Box example");
        window.setOnCloseRequest(event -> {
            event.consume();
            closeProgram();
        });

        button = new Button("Click Me");
      //  button.setOnAction(event -> AlertBox.display("Alert", "1000 bugs found"));
        button.setOnAction(event -> {
           boolean result =  ConfirmBox.display("Title of Window", "Waste your life on unnecessary things?");
            System.out.println(result);
        });
        StackPane layout = new StackPane();
        layout.getChildren().add(button);
        Scene scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }

    private void closeProgram() {
        boolean answer = ConfirmBox.display("Exit", "Are you sure?");
        if(answer) Platform.exit();
    }
}
