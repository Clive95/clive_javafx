package thenewboston.choicebox;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    Stage window;
    Scene scene;
    Button button;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("ChoiceBox example");

        button = new Button("Order");

        //ChoiceBox
        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        //getItems returns ObservableList object which you can add items to
        choiceBox.getItems().addAll("Apples", "Bananas", "Meatballs", "Memes");
        //default value
        choiceBox.setValue("Apples");

        // Listen for selection changes
        choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> System.out.println(newValue));
        button.setOnAction(event -> getChoice(choiceBox));

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20,20,20,20));
        layout.getChildren().addAll(button, choiceBox);

        scene = new Scene(layout, 350, 200);
        window.setScene(scene);
        window.show();
    }

    private void getChoice(ChoiceBox<String> choiceBox) {
        String food = choiceBox.getValue();
        System.out.println("Order: " + food);
    }
}
